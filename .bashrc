# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# secret 10x dev mode
set -o vi 

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

#if [ "$color_prompt" = yes ]; then
    #PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
#else
    #PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#fi
unset color_prompt force_color_prompt

PS1="\[\033[33m\]\w\[\033[33m\]>\[\033[00m\] "

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*|st*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@ian: \w\a\]$PS1"
    ;;
*)
    ;;
esac

[ -d "$HOME/bin" ] && PATH="$HOME/bin:$PATH"
[ -d "/usr/local/go/bin" ] && PATH="/usr/local/go/bin:$PATH"
[ -d "$HOME/go/bin" ] && PATH="$HOME/go/bin:$PATH"

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

#export devkitpro=~/devkitpro
#export devkitsnes=$devkitpro/devkitsnes
#export devkitmips=$devkitpro/devkitmips
#export devkitarm=$devkitpro/devkitarm

#export PATH=$PATH:$devkitarm/bin:$devkitmips/n64chain/tools/bin:$devkitsnes/bin

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep -P --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# some more ls aliases
alias ll='ls -hAlF'
alias la='ls -Ap'
alias l='ls -CF'

alias vfz='nvim $(fzf)'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi


base16sh() {
    if [ -n "$1" ]; then
    export BASE16_COLOR="base16-$1"
    export BASE16_SHELL="$HOME/.config/base16-shell/scripts/$BASE16_COLOR.sh"
    [[ -s $BASE16_SHELL ]] && source $BASE16_SHELL
    fi
}

BASE16_COLORS=(atelier-estuary-light atelier-lakeside-light atelier-savanna-light atelier-plateau-light cupcake darktooth grayscale-light gruvbox-dark-medium gruvbox-dark-soft harmonic-light material mocha monokai papercolor-light summerfruit-light solarized-light vulcan woodland)
complete -W "${BASE16_COLORS[@]}" base16sh
#export BASE16_COLOR="base16-${BASE16_COLORS[$((0 + RANDOM % ${#BASE16_COLORS[@]}))]}"
#export BASE16_COLOR="base16-monokai"
export BASE16_COLOR="base16-atelier-savanna-light"
#export BASE16_COLOR="base16-atelier-estuary-light"
#export BASE16_COLOR="base16-mocha"
#export BASE16_COLOR="base16-vulcan"
#export BASE16_COLOR="base16-harmonic-light"
export BASE16_SHELL="$HOME/.config/base16-shell/scripts/$BASE16_COLOR.sh"
[[ -s $BASE16_SHELL ]] && source $BASE16_SHELL
#export set TERM=screen-256color

alias objdump='objdump -M intel'
alias ghci='stack exec ghci'
alias runhaskell='stack runhaskell'
#alias clang='clang'

#[ -f ~/.fzf.bash ] && source ~/.fzf.bash
#alias fzf='~/bin/fzf/bin/fzf'
#alias fzf-tmux='~/bin/fzf/bin/fzf-tmux'

# fuzzy grep open via ag
fe() {
  local files
  IFS=$'\n' files=($(fzf-tmux --query="$1" --multi --select-1 --exit-0))
  [[ -n "$files" ]] && ${EDITOR:-vim} "${files[@]}"
}

export JBOSS_DEPLOY=/home/sdf/projects/workspace/wild/wildfly-14.0.1.Final/standalone/deployments
export P4PORT=perforce:1666
export P4CHARSET=utf8

export JAVA_HOME=/opt/jdk20
export PATH=$JAVA_HOME/bin:$PATH

[ -f "/home/deb/.ghcup/env" ] && source "/home/deb/.ghcup/env"

[ -f "$HOME/.cargo/env" ] && . "$HOME/.cargo/env"

