set nocompatible
filetype off
syntax off

set runtimepath+=~/.vim

let g:plug_url_format = "https://github.com/%s"
call plug#begin('~/.vim/plugged')
"Plug 'natebosch/vim-lsc'
"Plug 'mileszs/ack.vim'
Plug 'Raimondi/delimitMate'
Plug 'scrooloose/nerdcommenter'
"Plug 'godlygeek/tabular'
"Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
Plug 'keith/investigate.vim'
Plug 'tpope/vim-surround'
" vcs
Plug 'tpope/vim-fugitive'
"Plug 'int3/vim-extradite'
Plug 'airblade/vim-gitgutter'
" lisps 
"Plug 'jpalardy/vim-slime'
"Plug 'junegunn/rainbow_parentheses.vim'
"Plug 'guns/vim-sexp', {'for': ['clojure', 'scheme', 'lisp']}
"Plug 'tpope/vim-fireplace', {'for': 'clojure'}
" web
"Plug 'pangloss/vim-javascript'
" asm
Plug 'ARM9/arm-syntax-vim'
Plug 'ARM9/snes-syntax-vim'
Plug 'ARM9/vim-gas'
Plug 'fedorenchik/fasm.vim'
Plug 'ARM9/mips-syntax-vim'
Plug 'ARM9/gb-syntax-vim'
call plug#end()

filetype plugin indent on
syntax on

if has("gui_running")
  set guioptions=egt
  if has("win32")
    set guifont=consolas:h11
  else
    set guifont=Monospace\ 11
  endif
  set guicursor+=a:blinkon0
  set lines=58 columns=90
  " disable error beep
  autocmd GUIEnter * set vb t_vb=
  " auto chdir
  autocmd BufEnter * silent! lcd %:p:h
else
  if has("win32")
    set term=xterm
    set t_Co=256
    let &t_AB="\e[48;5;%dm"
    let &t_AF="\e[38;5;%dm"
    inoremap <Esc>[62~ <C-X><C-E>
    inoremap <Esc>[63~ <C-X><C-Y>
    nnoremap <Esc>[62~ <C-E>
    nnoremap <Esc>[63~ <C-Y>
  else
    set t_ut=
  endif
  let base16colorspace=256
endif

color base16-monokai

let mapleader=" "

set mouse=a
if has('mouse_sgr')
  set ttymouse=sgr
endif

" betterer set autochdir
" autocmd BufEnter * silent! lcd %:p:h
nnoremap <leader>cd :lcd %:p:h<CR>:pwd<CR>

set encoding=utf-8
set termencoding=utf-8
set fileformat=unix

set scrolloff=1
set shiftwidth=4 softtabstop=4 tabstop=8
set shiftround      " > and < round to nearest sw
set expandtab       " Expand tabs to spaces
set autoindent      " Auto indent with tabs or spaces
set wrap            " word wrap
set lbr             " 'smart' wrap
set cc=80           " vertical bar at column 80
" allow backspacing over everything in insert mode
set backspace=indent,eol,start
set history=50      " keep 50 lines of command line history
set number          " show line numbers
set ruler           " show the cursor position all the time
set showcmd         " display incomplete commands
set incsearch       " do incremental searching
set hidden          " hide buffers with ctrl-w o and ctrl-w c
set hlsearch

set tags=tags;,tags;~

set wildmenu        " ex completion suggestions in statusline 
set wildignore=*.swp,*.bak,*~
set wildignore+=*.o,*.min.*
set wildignore+=*/.git/**/*,*/.hg/**/*,*/.svn/**/*
set wildignore+=tags
set wildignorecase
set wildmode=list:full

set nrformats-=octal

set noerrorbells
set vb t_vb=

" sessions
set ssop-=options
set ssop-=folds

if has("vms")
  set nobackup      " do not keep a backup file, use versions instead
else
  set backup        " keep a backup file (*~)
endif

" Resume where you left off
autocmd BufReadPost *
  \ if line("'\"") > 1 && line("'\"") <= line("$") |
  \   exe "normal! g`\"" |
  \ endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
          \ | wincmd p | diffthis
endif

" auto-resize
" autocmd WinEnter * execute 'vertical resize ' . printf("%.0f", &columns * 0.60)

" Leader
map <space> <leader>
map <space><space> <leader><leader>

inoremap qk <esc>
noremap ; :
" Turns off highlight words after search
au BufEnter * nnoremap <buffer><nowait><silent> <esc> <esc>:noh<cr>
nnoremap <esc><esc> <nop>
nnoremap <silent> <esc> :noh<cr><esc>

" Don't use Ex mode, use Q for formatting
noremap Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

nnoremap Y y$
nnoremap yaa ggVG"+y''

noremap <C-q> <nop>
noremap <F1> <nop>
inoremap <F1> <nop>
nnoremap [[ <nop>
nnoremap ]] <nop>

" sort line(s)
vnoremap <F2> d:execute 'normal i' . join(sort(split(getreg('"'))), ' ')<cr>

vnoremap <tab> >gv
vnoremap <s-tab> <gv
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" searching
nnoremap <C-f> /\v\c
inoremap <C-f> <esc>/\v\c
" find & replace :%s/\c\v
nnoremap <leader>fr :%s/\v\c
nnoremap <leader>rw :%s/\<<C-r><C-w>\>//g<left><left>

" anti-clobber mappings
nnoremap c "_c
nnoremap C "_C

" Leader mappings
noremap <leader>y "+y
noremap <leader>Y "+Y
noremap <leader>p "+p
noremap <leader>P "+P
noremap <leader>d "_d
noremap <leader>D "_D

" buffer jumping
nnoremap gb :ls<cr>:buffer<Space>
nnoremap gB :ls<cr>:sbuffer<Space>
nnoremap <c-j> :bprev<cr>
nnoremap <c-k> :bnext<cr>

" find files
nnoremap <leader>ff :find *
nnoremap <leader>fs :sfind *
nnoremap <leader>fv :vert sfind *

nnoremap <leader>ts :%s/\s\+$//
" net-rw
command! -nargs=0 VX Vex | wincmd H | vert res 23<cr>
command! -nargs=0 VXR Vex | wincmd L | vert res 24<cr>

" error jumping
nnoremap <silent> [q :cprev<cr>zvzz
nnoremap <silent> ]q :cnext<cr>zvzz
nnoremap <silent> [Q :cfirst<cr>zvzz
nnoremap <silent> ]Q :clast<cr>zvzz
nnoremap <silent> [l :lprev<cr>zvzz
nnoremap <silent> ]l :lnext<cr>zvzz

command! -bar -bang -nargs=? -complete=buffer BD exe 'b ' . (empty(<q-args>) ? '#' : <q-args>) . '|bd<bang>#'

" sharing is caring
command! -range=% VP exe <line1> . "," . <line2> . "w !vpaste ft=" . &ft
vmap vp <ESC>:exe "'<,'>w !vpaste ft=".&ft<CR>

command! -nargs=? Hex exe 'e ' . <q-args> . " | setl binary | %!xxd -g 1"
command! HD exe "%!xxd -g 1"
command! HDR exe "%!xxd -r"

" remove all <tags>
command! -nargs=0 StripTags exe '%s/<.\{-}>//g'

nnoremap <leader>vrc :sp ~/.vimrc<cr>
nnoremap <leader>vre :sp ~/.emacs<cr>

let g:slime_target = "conemu"
"let g:slime_paste_file = tempname()
let g:slime_default_config = {"socket_name": "default", "target_pane": "1"}
let g:slime_dont_ask_default = 1

let g:make='make -j4'
let g:make_run='make run'

nnoremap <leader>ss :SlimeSend1<space>

nnoremap <silent> <leader>mk :exec ":SlimeSend1 " . g:make<cr>
nnoremap <silent> <leader>md :SlimeSend1 make -j9 debug<cr>
nnoremap <silent> <leader>mr :exec ":SlimeSend1 " . g:make_run<cr>
nnoremap <silent> <leader>mar :exec ":SlimeSend1 " . (g:make . " && " . g:make_run)<cr>
nnoremap <silent> <leader>mc :SlimeSend1 make clean<cr>

" folding
setlocal foldmethod=indent foldlevel=9001
set foldopen=block,hor,insert,jump,mark,percent,quickfix,search,tag,undo

" completion stuff
"inoremap <expr><C-space> <C-space>
set pumheight=10

let g:UltiSnipsExpandTrigger="<c-k>"
let g:UltiSnipsJumpForwardTrigger="<c-k>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"
" let g:UltiSnipsEditSplit="vertical"

" Autocomplete stuff
let g:lsc_enable_autocomplete  = v:true
let g:lsc_reference_highlights = v:true
let g:lsc_enable_diagnostics   = v:true
"let g:lsc_trace_level          = 'off'
set completeopt=menu,menuone,popup,noinsert,noselect

let g:lsc_auto_map = {
    \ 'GoToDefinition': 'gd',
    \ 'GoToDefinitionSplit': ['<C-W>]', '<C-W><C-]>'],
    \ 'FindReferences': 'gr',
    \ 'NextReference': '<C-n>',
    \ 'PreviousReference': '<C-p>',
    \ 'FindImplementations': 'gi',
    \ 'FindCodeActions': 'ga',
    \ 'Rename': 'gr',
    \ 'ShowHover': v:true,
    \ 'DocumentSymbol': 'go',
    \ 'WorkspaceSymbol': 'gs',
    \ 'SignatureHelp': 'gm',
    \ 'Completion': 'completefunc',
    \}

let g:lsc_server_commands = {
  \ 'fsharp': 'fsautocomplete',
  \ 'go': {
  \   "command": "gopls serve",
  \   "log_level": -1,
  \   "suppress_stderr": v:true,
  \ },
  \ 'javascript': 'typescript-language-server --stdio',
  \ 'typescript': 'typescript-language-server --stdio',
  \}

" --- Lauguage specifics

fun SetJavascriptOptions()
  setl sw=4 sts=4 ts=8
  setl expandtab
endfun
au FileType javascript call SetJavascriptOptions()

" refactor and make c/++ version
let s:cargo_build_mode = ''
command! -nargs=* CargoSlimeSend exe "SlimeSend1 cargo " . <q-args> . " " . s:cargo_build_mode

fun CargoSwitchMode()
  if s:cargo_build_mode == '--release'
    let s:cargo_build_mode = ''
    echo "debug build"
  else
    let s:cargo_build_mode = '--release'
    echo "release build"
  endif
endfun

fun SetRustOptions()
  nnoremap <leader>md :call CargoSwitchMode()<cr>
  nnoremap <silent> <leader>mk :CargoSlimeSend build<cr>
  nnoremap <silent> <leader>mr :CargoSlimeSend run<cr>
  nnoremap <silent> <leader>mc :CargoSlimeSend clean<cr>
endfun
au FileType rust call SetRustOptions()

" C++
fun SetCCXXOptions()
  syn match cBetterInts /\<v\?[us]\%(8\|16\|32\|64\)\>/
  hi def link cBetterInts Type
endfun
au FileType cpp,c call SetCCXXOptions()

" filetype autocmd
au BufNewFile,BufRead *.clj set filetype=clojure
au BufNewFile,BufRead *.asm,*.inc,*.fsasm set filetype=snes
au BufNewFile,BufRead *.s set filetype=gas

au FileType text,markdown,git,gitcommit,gitrebase,gitsendemail setlocal spell textwidth=78

let g:NERDCustomDelimiters = {
    \ 'snes': { 'left': ';' },
    \ 'arm': { 'left': '//', 'leftAlt': '/*', 'rightAlt': '*/' },
    \ 'mips': { 'left': '//', 'leftAlt': '/*', 'rightAlt': '*/' },
    \ 'gas': { 'left': '//', 'leftAlt': '/*', 'rightAlt': '*/' } }

" rainbow parens
"let g:rainbow#max_level = 16
"let g:rainbow#pairs = [['(',')'], ['[',']']]
"au FileType * RainbowParentheses

let g:ackprg = "ag"
"let g:ack_default_options = '--column'
nnoremap <leader>ag :Ack 
nnoremap <leader>gr :Ack 

" Print syntax group info
command! WhichHigroup :echo "hi<" . 
            \ synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . 
            \ synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . 
            \ synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"

" expendable n mappings:
" expendable i mappings:
" <c-b>
" <c-l>

" vim: sts=2 sw=2
