vim.g.mapleader = ' '
vim.g.maplocalleader = ' '
vim.opt.keywordprg = ''
vim.opt.timeoutlen = 4000

vim.cmd [[packadd packer.nvim]]
require('packer').startup(function(use)
  use 'wbthomason/packer.nvim'

  use 'tpope/vim-surround'
  use 'tpope/vim-commentary'
  --use 'tpope/vim-dispatch'
  use 'tpope/vim-fugitive'
  use 'airblade/vim-gitgutter'

  use 'neovim/nvim-lspconfig'
  use 'hrsh7th/cmp-nvim-lsp'
  use 'hrsh7th/nvim-cmp'
  use 'hrsh7th/cmp-buffer'
  --use 'hrsh7th/cmp-path'
  use 'hrsh7th/cmp-cmdline'
  use 'simrat39/symbols-outline.nvim'

  use 'dcampos/nvim-snippy'
  use 'dcampos/cmp-snippy'

  use 'Vigemus/iron.nvim'

  --[[use { 'github/copilot.vim',
        ft = {'c', 'cpp', 'rust', 'java', 'clojure', 'haskell', 'php', 'python', 'go', 'lua', 'typescript', 'javascript'},
        cmd = ':Copilot enable',
        run = ':Copilot setup' }--]]

  use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }

  use 'bradcush/base16-nvim'
  use 'savq/melange-nvim'

end)

vim.opt.termguicolors = true
--vim.opt.background = 'dark'
--vim.cmd.colorscheme 'melange'
--vim.cmd.colorscheme 'base16-mocha'
--vim.cmd.colorscheme 'base16-harmonic-light'
--vim.cmd.colorscheme 'base16-atelier-savanna-light'
--vim.cmd.colorscheme 'base16-vulcan'
vim.cmd.colorscheme (os.getenv("BASE16_COLOR") or 'base16-mocha')

local map = vim.keymap.set
map('n', '<leader>vrc', ':sp ' .. vim.fn.stdpath("config") .. '/init.lua<cr>')

vim.opt.fileencoding = "utf-8"
vim.opt.fileformat = "unix"

vim.opt.number = true
--vim.opt.smartindent = true
vim.opt.expandtab = true
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.tabstop = 8
vim.opt.lbr = true

vim.opt.scrolloff = 4
vim.opt.colorcolumn = "80"

vim.opt.hlsearch = true
vim.opt.incsearch = true

vim.opt.pumheight = 10

map('i', 'qk', '<esc>', { desc = "Exit insert mode" })
map('', ';', ':', { desc = "Remap ; to :" })
map('', 'q;', 'q:', { desc = "Also remap q; to q:" })
vim.cmd(":tnoremap <esc> <C-\\><C-n>")
vim.cmd(":tnoremap qk <C-\\><C-n>")

-- unbind useless binds
map('', '<F1>', '<nop>')
map('', 'Q', '<nop>')
map('n', '[[', '<nop>'); map('n', ']]', '<nop>')
map('n', '][', '<nop>'); map('n', '[]', '<nop>')

map('', '<leader>y', '"+y')
map('', '<leader>Y', '"+Y')
map('n', '<leader>y ', ':%y+<cr>')
map('', '<leader>p', '"+p')
map('', '<leader>P', '"+P')
map('', '<leader>d', '"_d')
map('n', 'c', '"_c')
map('n', 'C', '"_C')


map('v', '<C-f>', '"xy/\\V<C-r>x<cr>')
map('n', '<leader>rw', ":%s/\\V\\<<C-r><C-w>\\>//g<left><left>")
map('n', '<leader>re', ":%s///g<left><left>")
map('v', '<leader>re', '"xy:%s/\\V<C-r>x//g<left><left>')

map('n', 'gb', ':ls<cr>:buffer<space>')
map('n', 'gB', ':ls<cr>:sbuffer<space>')
map('n', '<c-j>', ':bprev<cr>')
map('n', '<c-k>', ':bnext<cr>')
vim.cmd("command! -nargs=0 BD exe 'bd!'")

map('n', '[q', ':cprev<cr>zvzz', {silent=true})
map('n', ']q', ':cnext<cr>zvzz', {silent=true})
map('n', '[Q', ':cfirst<cr>zvzz', {silent=true})
map('n', ']Q', ':clast<cr>zvzz', {silent=true})
map('n', '[l', ':lprev<cr>zvzz', {silent=true})
map('n', ']l', ':lnext<cr>zvzz', {silent=true})

-- move to enclosing brackets
map('n', '{', '[{', {silent=true})
map('n', '}', ']}', {silent=true})
map('n', '[[', '[%', {silent=true})
map('n', ']]', ']%', {silent=true})

map('v', '<tab>', '>gv')
map('v', '<s-tab>', '<gv')
map('v', 'J', ":m '>+1<CR>gv=gv")
map('v', 'K', ":m '<-2<CR>gv=gv")

map('n', '<leader>cd', ':lcd %:p:h<cr>:pwd<cr>')

map('n', '<esc>', '<esc>:noh<cr>', {nowait=true,silent=true})

vim.cmd("command -nargs=+ Ggr execute 'Ggrep! -q' <q-args>")
map('n', '<leader>gr', ':Ggr ')

vim.opt.wildignore = {
    "*.swp",
    "*~",
    ".*~",
    "*/.git/**/*",
    "*/.hg/**/*",
    "*/.svn/**/*"
}

-- todo figure out how fix title in nvim-qt
-- if vim.somethingsomething.has('gui') ?????
-- vim.cmd(':set title titlestring=%(%{expand(\"%:~:.:h\")}%)/%t')
vim.cmd([[
command! WhichHigroup :echo "hi<" . 
            \ synIDattr(synID(line("."),col("."),1),"name") . '> trans<' . 
            \ synIDattr(synID(line("."),col("."),0),"name") . "> lo<" . 
            \ synIDattr(synIDtrans(synID(line("."),col("."),1)),"name") . ">"
]])

require("symbols-outline").setup {
    highlight_hovered_item = true,
}

-- neoslime
vim.g.make_profile = ''
vim.g.make = ':Make -j4 %s'
vim.g.make_run = ':Start make run'
vim.g.make_clean = ':Make clean'
vim.g.make_test = ':Start make test'

local function send_to_tmux(cmd)
    vim.cmd('silent :exe \'!tmux send -t 1 "' .. cmd .. '" Enter\'')
end

map('n', '<leader>mp', function()
    if vim.g.make_profile == '' then
        vim.g.make_profile = 'debug'
        print('debug profile')
    else
        vim.g.make_profile = ''
        print('release profile')
    end
end, {silent=false})

map('n', '<leader>mk', function()
    vim.cmd(string.format(vim.g.make, vim.g.make_profile))
    --vim.cmd(':make -j4')
    --send_to_tmux(vim.b.make or vim.g.make)
end, {silent=true})

map('n', '<leader>mr', function()
    if vim.g.make_profile == 'debug' then
        vim.cmd(':Start make run-debug')
    else
        vim.cmd(vim.g.make_run)
    end
end, {silent=true})

map('n', '<leader>mcl', function()
    vim.cmd(vim.g.make_clean)
end, {silent=true})

map('n', '<leader>mt', function()
    vim.cmd(vim.g.make_test);
end, {silent=true})

local function add_build_tool(m)
    vim.g.make=m['make']
    vim.g.make_run=m['run']
    vim.g.make_clean=m['clean']
    vim.g.make_test=m['test']
end

--[[vim.api.nvim_create_autocmd('FileType',{pattern='cpp',callback=function()
    map('n', '<leader>mm', ':! curl localhost:8000/reload<cr><cr>', {silent=true, buffer=true})
end})]]

vim.api.nvim_create_autocmd('FileType', {pattern='rust',callback=function()
    add_build_tool({make='cargo build %s',run='cargo run',clean='cargo clean',test='cargo test'})
end})

vim.api.nvim_create_autocmd('FileType',{pattern='java,jsp',callback=function()
    vim.cmd.colorscheme 'base16-tomorrow-night'
    map('n', 'K', '<nop>', {silent=true, buffer=true})
end})

--[[function CopyJS()
    lines = vim.api.nvim_buf_get_lines(0,0,-1,false)
end]]

vim.api.nvim_create_autocmd('FileType',{pattern='javascript,typescript',callback=function()
    add_build_tool({make='npm run build',run='npm start',clean='echo no cleaning',test='npm test'})
end})

--vim.api.nvim_create_autocmd('FileType',{pattern='php',callback=function()
    --vim.cmd('setlocal noexpandtab sw=4 sts=4 ts=4')
--end})

