local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[e', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']e', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
    -- Enable completion triggered by <c-x><c-o>
    vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions
    local bufopts = { noremap=true, silent=true, buffer=bufnr }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
    vim.keymap.set('n', '<leader>k', vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set('n', '<leader>wa', vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set('n', '<leader>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set('n', '<leader>wl', function()
        print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
    end, bufopts)
    vim.keymap.set('n', '<leader>D', vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set('n', '<leader>rn', vim.lsp.buf.rename, bufopts)
    vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
    vim.keymap.set('n', '<leader>=', function() vim.lsp.buf.format { async = true } end, bufopts)
end

local cmp = require'cmp'

cmp.setup({
    formatting = {
        format = function(entry, vim_item)
            local max_width = math.floor(vim.api.nvim_get_option('columns') * 0.35)-- 60
            local label = vim_item.abbr
            local trunc_label = vim.fn.strcharpart(label, 0, max_width)
            if trunc_label ~= label then
                vim_item.abbr = trunc_label .. '…'
            elseif string.len(label) < max_width then
                local padding = string.rep(' ', max_width - string.len(label))
                vim_item.abbr = label .. padding
            end
            return vim_item
        end,
    },
    snippet = {
        expand = function(args)
            require('snippy').expand_snippet(args.body)
        end,
    },
    window = {
        -- completion = cmp.config.window.bordered(),
        -- documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
        --['<Tab>'] = nil,--[[function(fallback)
        --    if cmp.visible() then
        --        cmp.select_next_item()
        --    else
        --        fallback()
        --    end
        --end,]]
        ['<Tab>'] = nil,
        ['<S-Tab>'] = nil,
        ['<C-u>'] = cmp.mapping.scroll_docs(-4),
        ['<C-d>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.abort(),
        ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
        { name = 'snippy' }
    }, {
        { name = 'buffer' },
    })
})

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources(--[[{
        { name = 'git' }, -- You can specify the `cmp_git` source if you were installed it.
    },]] {
        { name = 'buffer' },
    })
})

-- Use buffer source for `/` and `?` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline({ '/', '?' }, {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
        { name = 'buffer' }
    }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})
--]]

-- Set up lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities()
local lspconfig = require('lspconfig')

local lsp_flags = {
    -- This is the default in Nvim 0.7+
    debounce_text_changes = 150,
}

capabilities.offsetEncoding = "utf-8"
lspconfig.clangd.setup {
    cmd = {'clangd-16'},
    capabilities = capabilities,
    on_attach = on_attach
}
local capabilities = require('cmp_nvim_lsp').default_capabilities()

--[[lspconfig.phpactor.setup{
    on_attach = on_attach,
    init_options = {
        ["language_server_phpstan.enabled"] = false,
        ["language_server_psalm.enabled"] = false,
    }
}]]

lspconfig.jdtls.setup{
    cmd = {
        'java',
        '-Declipse.application=org.eclipse.jdt.ls.core.id1',
        '-Dosgi.bundles.defaultStartLevel=4',
        '-Declipse.product=org.eclipse.jdt.ls.core.product',
        '-Dlog.protocol=true',
        '-Dlog.level=ALL',
        '-Xmx1g',
        '-jar', '/home/deb/bin/jdtls/plugins/org.eclipse.equinox.launcher_1.6.500.v20230717-2134.jar',
        '-configuration', '/home/deb/bin/jdtls/config_linux',
        '-data', '/home/deb/bin/jdtls/workspace'
    },
    on_attach = on_attach,
    capabilities = capabilities,
    flags = lsp_flags,
    root_dir = function(fname)
        return lspconfig.util.root_pattern('pom.xml', 'gradle.build', '.git')(fname) or lspconfig.util.path.dirname(fname)
    end,
    settings = {
        java = {
            signatureHelp = { enabled = true },
            sources = {
                organizeImports = {
                    starThreshold = 9999,
                    staticStarThreshold = 9999
                }
            },
            codeGeneration = {
                toString = {
                    template = '${object.className}{${member.name()}=${member.value}, ${otherMembers}}',
                    generateComments = true
                }
            },
            configuration = {
                runtimes = {
                    {
                        name = 'JavaSE-20',
                        path = '/opt/jdk20/',
                    }
                }
            }
        }
    }
}
lspconfig.clojure_lsp.setup{
    cmd = {'clojure-lsp'},
    on_attach = on_attach,
    capabilities = capabilities
}
lspconfig.hls.setup{
    --cmd = {'haskell-language-server-wrapper', '--lsp'},
    on_attach = on_attach,
    capabilities = capabilities
}
lspconfig['gopls'].setup{
    cmd = {'gopls'},
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        gopls = {
            experimentalPostfixCompletions = true,
            analyses = {
                unusedparams = true,
                shadow = true,
            },
            staticcheck = true,
        },
    },
    init_options = {
        usePlaceholders = true,
    }
}
lspconfig.tsserver.setup{
    on_attach = on_attach,
    capabilities = capabilities,
    settings = {
        implicitProjectConfig = {
            checkJs = true
        }
    }
}
--[[lspconfig.pyright.setup{
    on_attach = on_attach,
    capabilities = capabilities
}]]
lspconfig['rust_analyzer'].setup{
    capabilities = capabilities,
    on_attach = on_attach,
    flags = lsp_flags,
    settings = {
        ["rust-analyzer"] = {}
    }
}

-- local snippy = require('snippy').setup({
--     mappings = {
--         is = {
--             ['<Tab>'] = 'expand_or_advance',
--             ['<S-Tab>'] = 'previous',
--         },
--     },
-- })
