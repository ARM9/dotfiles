
require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  ensure_installed = { "lua", "go", "typescript", "vim"},

  sync_install = false,

  auto_install = false,

  highlight = {
    enable = true,

    disable = function(lang, buf)
        local max_filesize = 200 * 1024 -- 200 KB
        local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
        if ok and stats and stats.size > max_filesize then
            vim.cmd("syntax off")
            return true
        end
        local no = {"javascript", "c"}
        for _,v in pairs(no) do
            if v == lang then
                vim.cmd("syntax on")
                return true
            end
        end
        return false
    end,

    additional_vim_regex_highlighting = false,
  },
}

