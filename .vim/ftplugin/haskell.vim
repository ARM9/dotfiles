
if has('python')
  command! -nargs=? -range=% Hoogle python hoogle_lookup("<args>")
else
  command! Hoogle echo 'Only avaliable with +python support.'
endif

if has('python')
python << EOF

def hoogle_lookup(query):
  import re
  import urllib
  import vim
  import webbrowser

  url = 'https://www.stackage.org/lts/hoogle?q='

  query.replace(' ', '+')
  url = url + query

  #response = urllib.urlopen(url, urllib.urlencode(data))
  #url = response.geturl()
  webbrowser.open(url)

EOF
endif
