(setq inhibit-startup-message t)
(setq initial-scratch-message nil)
(setq ring-bell-function 'ignore)

(require 'package)
(add-to-list 'package-archives '("marmalade" . "http://marmalade-repo.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.milkbox.net/packages/"))
(package-initialize)

(defvar my-packages '(flycheck
                      company
                      yasnippet ;yasnippet-snippets
                      magit
                      slime
                      irony company-irony flycheck-irony ;irony-eldoc
                      ;fsharp-mode
                      ;elixir-mode alchemist flycheck-elixir
                      ;intero
                      rainbow-delimiters
                      cider
                      key-chord
                      evil evil-leader evil-nerd-commenter evil-surround
                      ))

(dolist (p my-packages)
  (when (not (package-installed-p p))
    (package-install p)))

; fuzz
(ido-mode t)
(ido-everywhere 1)
(setq ido-use-faces nil)
(setq ido-enable-flex-matching t)
(setq ido-use-filename-at-point nil)
(setq ido-auto-merge-work-directories-length -1)
(setq ido-use-virtual-buffers t)

; emacs stuff
(setq-default indent-tabs-mode nil)
;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; don't accelerate scrolling
;(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1)

(show-paren-mode 1)
(setq initial-frame-alist '((top . 0) (left . 0) (width . 90) (height . 53)))
(global-linum-mode t)

; fix regex search
(global-set-key (kbd "C-s") 'isearch-forward-regexp)
(global-set-key (kbd "C-r") 'isearch-backward-regexp)
(global-set-key (kbd "C-M-s") 'isearch-forward)
(global-set-key (kbd "C-M-r") 'isearch-backward)

(define-key global-map (kbd "RET") 'newline-and-indent)

(fset 'yes-or-no-p 'y-or-n-p)

; evil settings
(evil-mode 1)
(global-evil-surround-mode 1)
(define-key evil-normal-state-map [escape] 'keyboard-quit)
(define-key evil-visual-state-map [escape] 'keyboard-quit)
(define-key minibuffer-local-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-ns-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-completion-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-must-match-map [escape] 'minibuffer-keyboard-quit)
(define-key minibuffer-local-isearch-map [escape] 'minibuffer-keyboard-quit)
(setq evil-regexp-search t)
; evil keys
(key-chord-mode 1)
(setq key-chord-two-keys-delay 1.0)
(key-chord-define evil-insert-state-map "qk" 'evil-normal-state)
(key-chord-define evil-normal-state-map "gb" 'switch-to-buffer)

(define-key evil-normal-state-map (kbd "C-j") 'previous-buffer)
(define-key evil-normal-state-map (kbd "C-k") 'next-buffer)

(define-key evil-motion-state-map (kbd ";") 'evil-ex)

(define-key evil-normal-state-map (kbd "<SPC>vre")
            '(lambda () (interactive) (find-file (concat (getenv "home") "/.emacs"))))

; evil-leader settings
(global-evil-leader-mode)
(evil-leader/set-leader "<SPC>")
;(evil-leader/set-key "gb" 'switch-to-buffer)
; evil-nerd-commenter settings
(evilnc-default-hotkeys)

; auto-complete
;(global-auto-complete-mode)
;(setq ac-delay 0.0)
;(setq ac-quick-help-delay 0.0)
;(setq ac-use-fuzzy 1)
;(ac-config-default)
(global-company-mode)
(setq company-idle-delay 0.1)
(setq company-minimum-prefix-length 2)
(add-hook 'after-init-hook #'global-flycheck-mode)
(setq flycheck-check-syntax-automatically '(save new-line idle-change))
(yas-global-mode 1)

; c/c++ settings
(setq gdb-many-windows t)
(add-hook 'c++-mode-hook 'irony-mode)
(add-hook 'c-mode-hook 'irony-mode)
(defun my-irony-mode-hook ()
  (define-key irony-mode-map [remap completion-at-point]
    'irony-completion-at-point-async)
  (define-key irony-mode-map [remap complete-symbol]
    'irony-completion-at-point-async))
(add-hook 'irony-mode-hook 'my-irony-mode-hook)
(add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

(add-hook 'irony-mode-hook
    '(lambda ()
        (eldoc-mode)
        (irony-eldoc)
        (eval-after-load 'company
            '(add-to-list 'company-backends 'company-irony))))

; lisp settings
(setq inferior-lisp-program "sbcl")
(setq slime-contribs '(slime-repl))

; haskell settings
;(add-hook 'haskell-mode-hook 'intero-mode)
;(add-hook 'haskell-mode-hook
          ;'(lambda ()
             ;(define-key evil-normal-state-map (kbd "M-.") #'intero-goto-definition)))
;(add-hook 'haskell-mode-hook 'company-mode)
;(add-hook 'haskell-interactive-mode-hook 'company-mode)
;(add-hook 'haskell-mode-hook 'eldoc-mode)

; fsharp settings
;(require 'fsharp-mode)

; clojure settings
(setq cider-auto-select-error-buffer nil)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(add-hook 'clojure-mode-hook 'turn-on-eldoc-mode)
(add-hook 'cider-interaction-mode-hook 'cider-turn-on-eldoc-mode)
(add-hook 'cider-repl-mode-hook #'company-mode)
;(add-hook 'cider-mode-hook #'company-mode)
(add-hook 'cider-mode-hook
          '(lambda ()
             ;(define-key evil-normal-state-map (kbd "M-.") #'racer-find-definition)
             ;(company-mode)
             (local-set-key (kbd "TAB") #'company-indent-or-complete-common)))

(setq cider-cljs-lein-repl
      "(do (require 'figwheel-sidecar.repl-api)
           (figwheel-sidecar.repl-api/start-figwheel!)
           (figwheel-sidecar.repl-api/cljs-repl))")
; themes
(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(load-theme 'base16-mocha-dark t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("8704829d51ea058227662e33f84313d268b330364f6e1f31dc67671712143caf" "978ff9496928cc94639cb1084004bf64235c5c7fb0cfbcc38a3871eb95fa88f6" "51bea7765ddaee2aac2983fac8099ec7d62dff47b708aa3595ad29899e9e9e44" "eb3c7a18d22bb860e040b260733b047ed7a863d0c0d32a4bb57c609821fd1428" "b6db49cec08652adf1ff2341ce32c7303be313b0de38c621676122f255ee46db" "e254f8e18ba82e55572c5e18f3ac9c2bd6728a7e500f6cc216e0c6f6f8ea7003" "3e421eba9067afd176ec5b530e5437f93d353e9e6a95290a870f063889d92be6" "134f38000f413a88743764983c751ac34edbec75a602065e2ae3b87fcf26c451" "03e3e79fb2b344e41a7df897818b7969ca51a15a67dc0c30ebbdeb9ea2cd4492" "f21caace402180ab3dc5157d2bb843c4daafbe64aadc362c9f4558ac17ce43a2" "41b6698b5f9ab241ad6c30aea8c9f53d539e23ad4e3963abff4b57c0f8bf6730" default)))
 '(package-selected-packages
   (quote
    (evil-surround evil-nerd-commenter evil-leader evil key-chord rainbow-delimiters intero flycheck-rust racer rust-mode magit company flycheck)))
 '(show-paren-mode t)
 '(tool-bar-mode nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Monospace" :foundry "unknown" :slant normal :weight normal :height 112 :width normal))))
 '(company-echo-common ((t (:foreground "gainsboro"))))
 '(company-preview ((t (:background "gray14" :foreground "wheat"))))
 '(company-preview-common ((t (:inherit company-preview :foreground "light gray"))))
 '(company-preview-search ((t (:inherit company-preview :background "gray20"))))
 '(company-scrollbar-bg ((t (:background "gray32"))))
 '(company-scrollbar-fg ((t (:background "gray"))))
 '(company-template-field ((t (:background "gray12" :foreground "gainsboro"))))
 '(company-tooltip ((t (:background "gray24" :foreground "gainsboro"))))
 '(company-tooltip-annotation ((t (:foreground "light gray"))))
 '(company-tooltip-common ((t (:foreground "dark gray"))))
 '(company-tooltip-selection ((t (:background "gray18")))))
