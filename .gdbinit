
#source /usr/local/lib/python2.7/dist-packages/voltron/entry.py
set disassembly-flavor intel
#set output-radix 0x10
#set input-radix 0x10

set $SHOWCPUREGISTERS = 1

define flags
  # OF (overflow) flag
  if (($eflags >> 0xB) & 1)
    printf "O "
    set $_of_flag = 1
  else
    printf "o "
    set $_of_flag = 0
  end
  if (($eflags >> 0xA) & 1)
    printf "D "
  else
    printf "d "
  end
  if (($eflags >> 9) & 1)
    printf "I "
  else
    printf "i "
  end
  if (($eflags >> 8) & 1)
    printf "T "
  else
    printf "t "
  end
  # SF (sign) flag
  if (($eflags >> 7) & 1)
    printf "S "
    set $_sf_flag = 1
  else
    printf "s "
    set $_sf_flag = 0
  end
  # ZF (zero) flag
  if (($eflags >> 6) & 1)
    printf "Z "
    set $_zf_flag = 1
  else
    printf "z "
    set $_zf_flag = 0
  end
  if (($eflags >> 4) & 1)
    printf "A "
  else
    printf "a "
  end
  # PF (parity) flag
  if (($eflags >> 2) & 1)
    printf "P "
    set $_pf_flag = 1
  else
    printf "p "
    set $_pf_flag = 0
  end
  # CF (carry) flag
  if ($eflags & 1)
    printf "C "
    set $_cf_flag = 1
  else
    printf "c "
    set $_cf_flag = 0
  end
  printf "\n"
end
document flags
Print flags register.
end

define eflags
  printf "     OF <%d>  DF <%d>  IF <%d>  TF <%d>",\
  (($eflags >> 0xB) & 1), (($eflags >> 0xA) & 1), \
  (($eflags >> 9) & 1), (($eflags >> 8) & 1)
  printf "  SF <%d>  ZF <%d>  AF <%d>  PF <%d>  CF <%d>\n",\
  (($eflags >> 7) & 1), (($eflags >> 6) & 1),\
  (($eflags >> 4) & 1), (($eflags >> 2) & 1), ($eflags & 1)
  printf "     ID <%d>  VIP <%d> VIF <%d> AC <%d>",\
  (($eflags >> 0x15) & 1), (($eflags >> 0x14) & 1), \
  (($eflags >> 0x13) & 1), (($eflags >> 0x12) & 1)
  printf "  VM <%d>  RF <%d>  NT <%d>  IOPL <%d>\n",\
  (($eflags >> 0x11) & 1), (($eflags >> 0x10) & 1),\
  (($eflags >> 0xE) & 1), (($eflags >> 0xC) & 3)
end
document eflags
Print eflags register.
end

define func
  if $argc == 0
    info functions
  end
  if $argc == 1
    info functions $arg0
  end
  if $argc > 1
    help func
  end
end
document func
Print all function names in target, or those matching REGEXP.
Usage: func <REGEXP>
end


define var
  if $argc == 0
    info variables
  end
  if $argc == 1
    info variables $arg0
  end
  if $argc > 1
    help var
  end
end
document var
Print all global and static variable names (symbols), or those matching REGEXP.
Usage: var <REGEXP>
end


define lib
  info sharedlibrary
end
document lib
Print shared libraries linked to target.
end

define mks
  make
  if $argc==0
  start
  end
  if $argc==1
  start $arg0
  end
  if $argc==2
  start $arg0 $arg1
  end
  if $argc==3
  start $arg0 $arg1 $arg2
  end
  # have mercy..
  if $argc==4
  start $arg0 $arg1 $arg2 $arg3
  end
  if $argc==5
  start $arg0 $arg1 $arg2 $arg3 $arg4
  end
  if $argc==6
  start $arg0 $arg1 $arg2 $arg3 $arg4 $arg5
  end
  if $argc==7
  start $arg0 $arg1 $arg2 $arg3 $arg4 $arg5 $arg6
  end
end
document mks
Invoke `shell make` and then `start`
end
